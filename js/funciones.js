var modal = {};
modal.open = function( modalData ){

	var title = '<div class="title">'+modalData.title+'</div>';
	var modalBody = '';
	$.ajax({
		type:'get',
		dataType:'html',
		url:modalData.url,
		cache:false,
		async:false,
		success:function( html ){
			modalBody = '<div class="body">'+html+'</div>';
		}
	})
	var htmlModal =  '<div class="modal"><div class="container">'+title+modalBody+'</div></div>';

	$(document.body).append( htmlModal );

	$(document).bind('keydown',function(e){
		if ( e.which == 27 ) {
        	modal.close();
        };
	});

}

modal.close = function(){
	$('.modal').remove();
}

var validatorForm = function( form ){
	var isValid = true;
	$( '#'+form+' *:not(script, style)' ).each( function(x, element){
		if( element.attributes && element.attributes.required){

			if( !element.value ){
				isValid = false;
				$(element).css('border','1px solid red');
				$(element).after( '</br><span id="error_'+element.attributes.id.value+'" style="color:red;">El campo es requerido.</span>' );
				$( element ).keypress(function(event) {
					/* Act on the event */
					$(element).css('border','1px solid #A4A6A8');
					$('#error_'+element.attributes.id.value).remove();

				});
				$( element ).change(function(event) {
					/* Act on the event */
					$(element).css('border','1px solid #A4A6A8');
					$('#error_'+element.attributes.id.value).remove();

				});
			}
		}
	});

	return isValid;

}

var listado = function( data ){

	$.ajax({
		type:'post',
		dataType:'json',
		url: data.url,
		data: data.data,
		cache:false,
		success:function( res ){	
			if( res && res.success ){
				$('#'+data.id).html('');
				$.each( res.rs , function( x, objData ){
					var tr = '';
					var td = '';
					$.each( data.row, function( i, row ){
						if( row.type == 'text' ){
							td += '<td '+row.attr+'>'+objData[row.value]+'</td>';
						}else if( row.type == 'button' ){
							var obj = '{';
							var count = 1;
							$.each( objData, function( k, v ){

								if( v == true ){
									v = '1'
								}else if( v == false )
									v = '0';
								else if(!v && v != false) 
									v = '';
							 	v = v.replace(/"/g,'')
							 	v = v.replace(/(?:\r\n|\r|\n)/g, ' ');
								if( count < Object.keys(objData).length )
									obj += k +":'"+v+"',";
								else
									obj += k +":'"+v+"'}";
								
								count ++;
							});
							var button = '<button type="button" class="'+row.class+'" onClick="'+row.click+'('+obj+')">'+row.text+'</button>';
							td += '<td '+row.attr+'>'+button+'</td>';
						}
					});
					var tr = '<tr>'+td+'</tr>';
					$('#'+data.id).append(tr);
				});
			}
		}
	});

}

var optionsSelect = function ( data ){
	
	$( '#'+data.selector ).html('<option value="">Seleccione...</option>');

	$.ajax({
		type:'post',
		dataType:'json',
		url: data.url,
		data: data.data,
		cache:false,
		async:false,
		success:function( res ){

			if( res.success ){
				
				if( res.rs.length ){
					$.each( res.rs, function( key, value ){
						$( '#'+data.selector ).append( '<option value="'+ value[data.value] +'">'+ value[data.display] +'</option>' );
					});
				}
		
			}else{
				console.log('Ha ocurrido un error', response.MESSAGE,'error', 5000 )
			}
		}
	});

}

var inputFloat = function(t){

	if ((event.which != 46 || $(t).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)){
		event.preventDefault();
	}
}
	