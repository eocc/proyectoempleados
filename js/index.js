listar();
function listar( ){

	listado({
		id:'tr-listado',
		url:'componentes/proxy.php',
		data:{
			component:'empleados',
			funcion:'leer',
			data:{}
		},
		row:[
			{type:'text', value:'NombreCompleto', 		attr:'align="center"'},
			{type:'text', value:'fNac', 		attr:'align="center"'},
			{type:'text', value:'Descripcion', attr:'align="center"'},
			{type:'text', value:'Sueldo', 		attr:'align="center"'},
			{type:'button', text:'Modificar', click:'abrirModal', class:'btn-guardar', attr:'align="center"'},
			{type:'button', text:'Eliminar', click:'eliminar', class:'btn-guardar', attr:'align="center"'}
		]
	});

}


function abrirModal( empleado ){

	modal.open({
		url:'guardar-empleado.html',
		title:'ABC Empleados'
	});

	optionsSelect({
		selector:'Departamento',
		url:'componentes/proxy.php',
		data:{
			component:'empleados',
			funcion:'leerDepartamentos',
			data:{}
		},
		value:'Puesto',
		display:'Descripcion'
	});

	if( empleado ){

		$.each( empleado, function( x, val ){
			$('#'+x).val(val);
		})
	}

}

function guardar(  ){

	if( validatorForm( 'formEmpleado' ) ){

		$.ajax({
			type:'post',
			dataType:'json',
			url:'componentes/proxy.php',
			data:{
				component:'empleados',
				funcion:'guardar',
				params:{
					clave_emp: 	  $('#clave_emp').val(),
					Nombre:    	  $('#Nombre').val(),
					ApPaterno: 	  $('#ApPaterno').val(),
					ApMaterno: 	  $('#ApMaterno').val(),
					FecNac:    	  $('#FecNac').val(),
					Departamento: $('#Departamento').val(),
					Sueldo:    	  $('#Sueldo').val()
				}
			},
			cache:false,
			success:function( res ){
				if( res.success ){
					alert('Se guardo el empleado correctament');
					listar();
					modal.close();
				}
			}
		})

	};

}

function eliminar( empleado ){

	if( confirm('¿Esta seguro de eliminar este empleado?') ){

		$.ajax({
			type:'post',
			dataType:'json',
			url:'componentes/proxy.php',
			data:{
				component:'empleados',
				funcion:'eliminar',
				params:{
					clave_emp: empleado.clave_emp
				}
			},
			cache:false,
			success:function( res ){
				if( res.success ){
					alert('Se eliminó el empleado correctament');
					listar();
				}
			}
		});

	}

}
