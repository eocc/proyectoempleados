<?php 
	
	class conexion{

	
		private function connect( $query, $list ){

			 $server = "localhost";
			 $user   = "root";
			 $pass = "";
			 $db = "proyectoEmpleados";
			
			$conn = mysqli_connect($server, $user, $pass, $db );
			if( !$conn ){
				$result = array( "success" => false, "message" => 'No se pudo conectar a la base de datos: ' . mysqli_error($conn));
			}

			if ( !mysqli_connect_errno() ){

				$rs = array(  );

		   		mysqli_query($conn,"START TRANSACTION");
				// echo $query;
		   		$execute = mysqli_query($conn,$query);
			    $error = mysqli_error($conn);

		   		
		   		if( $execute ){

		   			if( $list ){
				   		while( $line = mysqli_fetch_array($execute, MYSQLI_ASSOC) ){
							array_push($rs, $line );
						}
						$result = array( "rs" => $rs, "success" => true );
		   			}else{
		   				$result = array( "success" => true );
		   			}
		   			
					mysqli_query($conn,"COMMIT");
				
		   		}else{
			    	
			    	 mysqli_query($conn, "ROLLBACK");
			    	$result = array( "success" => false, "message" => 'Error en acceso a base de datos:'. $error );			   			
		   		}
				
			}else if( !$dbcon ){
				$result = array( "success" => false, "message" => 'La base dde datos seleccionada no existe.' );
			} 

			mysqli_close($conn);
			return $result;
			
		}

		public function exec( $query, $params, $list = true ){

			if( $params ){

				$values = '';
				for( $x =  0; $x < count($params); $x++ ){
				 	if( $x < count($params) -1 ){
				 		if( isset($params[$x]) ){
				 			$values = $values. '"' .$params[$x]. '"' .',';
				 		}else{
				 			$values = $values. 'null' .',';
				 		}
				 	}else{
				 		if( isset($params[$x]) ){
				 			$values = $values. '"' .$params[$x]. '"';	
				 		}else{
				 			$values = $values. 'null';	
				 		}
				 	}
				}
				$query = 'call '. $query.'('.$values.')';
			}else{
				$query = 'call '. $query.'()';
			}

			$conexion = new conexion();
			$result = $conexion->connect( $query, $list );

			return $result;

		}

	}

?>