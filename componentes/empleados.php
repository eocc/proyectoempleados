<?php 

	class empleados{

		public function leer( $params ){

			$conexion = new conexion();
			$data = array();
			$result = $conexion->exec( 'empleados_leer', $data );
			return $result;

		}

		public function leerDepartamentos( $params ){

			$conexion = new conexion();
			$data = array();
			$result = $conexion->exec( 'departamentos_leer', $data );
			return $result;


		}

		public function guardar( $params ){

			$conexion = new conexion();
			$data = array(
				$params['clave_emp'] ? $params['clave_emp'] : null,
				$params['Nombre'],
				$params['ApPaterno'],
				$params['ApMaterno'],
				$params['FecNac'],
				$params['Departamento'],
				$params['Sueldo'],
			);
			$result = $conexion->exec( 'empleados_guardar', $data, false );
			return $result;


		}

		public function eliminar( $params ){

			$conexion = new conexion();
			$data = array(
				$params['clave_emp'] 
			);
			$result = $conexion->exec( 'empleados_eliminar', $data, false );
			return $result;


		}

	}

?>